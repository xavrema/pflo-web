import React, { Component } from 'react';
import { BrowserRouter, Route, Switch } from 'react-router-dom';
import Main from './Main';
import NavBar from './Navbar';
import Portfolio from './Portfolio';
import Projects from './Projects';
import About from './About';
import ProjectInfo from './ProjectInfo';
import { library } from '@fortawesome/fontawesome-svg-core';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faBolt } from '@fortawesome/free-solid-svg-icons';
import { fab } from '@fortawesome/free-brands-svg-icons';
library.add(faBolt, fab);
class App extends Component {
  render() {
    return (
      <BrowserRouter>
        <div>
          <NavBar />
          <Switch >
            <Route path="/projects/:id" component={ProjectInfo} />
            <Route path="/portfolio" component={Portfolio} />
            <Route path="/projects" component={Projects} />
            <Route path="/about" component={About} />
            <Route path="/" component={Main} />
          </Switch>
        
        </div>
      </BrowserRouter>
    );
  }
}

export default App;
