import React, { Component } from 'react';
import { Grid, Col, Row } from 'react-bootstrap';
import '../App.css';

class Main extends Component {
  render() {
    return (
      <Grid>
        <Row className="bg_text">
          <Col xs={12}>
            <div className="bg_header_lg">
              &nbsp;
              {'< Hello World />'}
            </div>
          </Col>
        </Row>
      </Grid>
    );
  }
}

export default Main;
