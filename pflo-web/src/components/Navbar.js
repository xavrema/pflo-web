import React, { Component } from 'react';
import { Navbar, Nav, NavItem, Grid, Row, Col } from 'react-bootstrap';
import '../App.css';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

class NavBar extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }
  render() {
    return (
      <div >
        <div className="bg_img"></div>
        <Navbar inverse fluid className="nav-bar border-0 navbar_item">
          <Navbar.Header center>
            <Navbar.Brand>
              <a href="/">
                <div className="navbar_item">
                  {' '}
                  Xavier Remacle <FontAwesomeIcon icon="bolt" />
                </div>
              </a>
            </Navbar.Brand>
            <Navbar.Toggle />
          </Navbar.Header>
          <Navbar.Collapse>
            <Nav padding-right pullRight>
              <NavItem eventKey={1} href="/portfolio">
                <div className="navbar_item"> Portforlio</div>
              </NavItem>
              <NavItem eventKey={2} href="/projects">
                <div className="navbar_item"> Projects</div>
              </NavItem>
              <NavItem eventKey={2} href="/about">
                <div className="navbar_item"> About Me</div>
              </NavItem>
            </Nav>
          </Navbar.Collapse>
        </Navbar>
        
      </div>
    );
  }
}

export default NavBar;
