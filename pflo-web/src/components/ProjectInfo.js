import React, { Component } from 'react';
import axios from 'axios';
import {
  Grid,
  Col,
  Row,
  Panel,
  ListGroup,
  ListGroupItem
} from 'react-bootstrap';
import '../App.css';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';


class About extends Component {
  constructor(props) {
    super(props);
    const recordID = this.props.match.params.id;
    this.state = {
      id: recordID,
      isLoaded: false,
      langLoaded: false,
      languages: [],
      error: false,
      data: '',
      details: []
    };

    this.renderLanguages = this.renderLanguages.bind(this);

    this.getProjectDetails();
    console.log('error status----->', this.state.error);
    if (!this.state.error) {
      this.getProjectLanguage();
    }
  }
  async getProjectLanguage() {
    var res = await axios
      .get(`https://gitlab.com/api/v4/projects/${this.state.id}/languages`)
      .then(res => {
        this.setState({
          languages: Object.keys(res.data),
          langLoaded: true
        });
      })
      .catch(error => {
        console.log('No error was caught---->', error);
        this.setState({ error: true, langLoaded: true, isLoaded: true });
      });
  }

  async getProjectDetails() {
    var res = await axios
      .get(`https://gitlab.com/api/v4/projects/${this.state.id}`)
      .then(res => {
        this.setState({
          details: Object.keys(res.data),
          isLoaded: true,
          data: res.data
        });
      })
      .catch(res => {
        this.setState({ error: true });
        console.log('here-------------------><', this.state.error);
      });
  }

  renderLanguages() {
    return this.state.languages.map(value => {
      return <div className="pg_headers">{value}</div>;
    });
  }

  renderDetails() {
    return <div className="pg_headers">{this.state.data.description}</div>;
  }

  render() {
    if (!this.state.isLoaded || !this.state.langLoaded) {
      return (
        <Grid>
          <Row>
            <Col xs={6} className="bg_text">
              <div className="pg_headers">Loading Project...</div>
            </Col>
          </Row>
        </Grid>
      );
    } else if (this.state.error) {
      return (
        <Grid>
          <Row>
            <Col xs={6} className="bg_text">
              <div className="pg_headers">Project Not Found</div>
            </Col>
          </Row>
        </Grid>
      );
    } else {
      console.log('----Details---->', this.state.details);
      console.log('----Languages-->', this.state.languages);
      return (
        <Grid fluid={true}>
          <Row className="proj-card">
            <Col xs={12}>
              <Panel fluid>
                <Panel.Heading className="panel-head">
                  Project {this.state.data.name}
                </Panel.Heading>
                <Panel.Body className="panel-main">
                  <div>Language</div>
                  <div className="panel_info" />
                  {this.state.languages}
                </Panel.Body>
                <Panel.Body className="panel-main">
                  <div>Description</div>
                  <div className="panel_info">
                    {this.state.data.description}
                  </div>
                </Panel.Body>
                <a style={{ textDecoration: 'none' }} href={`https://gitlab.com/xavrema/${this.state.data.name}`}>
                <Panel.Body className="panel-main">
                  <div className="panel_info"><FontAwesomeIcon icon="gitlab" />{this.state.data.name}</div>
                </Panel.Body>
                </a>
              </Panel>
            </Col>
          </Row>
        </Grid>
      );
    }
  }
}

export default About;
