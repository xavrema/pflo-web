import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import {
  Grid,
  Col,
  Row,
  Panel,
  ListGroup,
  ListGroupItem
} from 'react-bootstrap';
import '../App.css';
import axios from 'axios';

class Projects extends Component {
  constructor(props) {
    super(props);
    this.state = { projs: [], languages: [], isLoaded: false };
  }
  async componentDidMount() {
    const test = await axios
      .get('https://gitlab.com/api/v4/users/xavrema/projects')
      .then(res => {
        const projs = res.data;
        this.setState({ projs, isLoaded: true });
      });
    this.getProjectLanguage();
  }

  async getProjectLanguage() {
    for (let p in this.state.projs) {
      var l = await axios.get(
        `https://gitlab.com/api/v4/projects/${
          this.state.projs[p]['id']
        }/languages`
      );
      this.setState({ languages: [...this.state.languages, l.data] });
    }
  }

  renderProjs() {
    return this.state.projs.map((value, index) => {
      console.log('---->Map Value', Object.keys(value));
        var languages = Object.keys(value);
      console.log('---->Lang map', this.state.languages[index]);
      return (
        <Col xs={6} key={value['id']}>
          <Link style={{ textDecoration: 'none' }} to={`/projects/${value['id']}`}>
            <Panel>
              <Panel.Heading className="panel-head">
                {value['name']}
              </Panel.Heading>

              <Panel.Body className="panel-main">{value.description}</Panel.Body>
            </Panel>
          </Link>
        </Col>
      );
    });    
  }

  render() {
    console.log(this.state.isLoaded, '--->isLoaded');
    if (this.state.error) {
      console.log('----->Error');
      return (
        <Grid>
          <Row className="bg_text">
            <Col xs={12}>
              <div className="bg_header_lg">Error :(</div>
            </Col>
          </Row>
        </Grid>
      );
    } else if (!this.state.isLoaded) {
      return (
        <Grid>
          <Row className="bg_text">
            <Col xs={12}>
              <div className="bg_header_lg">Connecting To Gitlab....</div>
            </Col>
          </Row>
        </Grid>
      );
    } else {
      console.log('language length------>', this.state.languages.length);
      return (
        <Grid fluid>
          <Row>
            <Col xsHidden sm={6} xs={12}>
              <div className="bg_header_sm">Projects</div>
            </Col>
            <Col sm={5} xs={12} >
              <Row className="proj-card">{this.renderProjs()}</Row>
            </Col>
          </Row>
        </Grid>
      );
    }
  }
}

export default Projects;
 